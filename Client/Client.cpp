#include <sys/types.h>
#include <stdlib.h>
#include "stdio.h"
#include "stdio_ext.h"
#include "sys/socket.h"
#include <netinet/in.h>
#include "unistd.h"
#include "string.h"
#include "fcntl.h"
#include <iostream>

using namespace std;

#define SOCKET_READ_TIMEOUT_USEC 1
#define MAX_MESSAGE_LENGTH 1024
#define NETWORK_PORT 3425

int PrepareClientSocket();
void InputMessage(char *message);
void PrintMenu();
void ExecuteClient();
void ExecuteSender(int sock);
void ExecuteListener(int sock);

int main()
{
    ExecuteClient();
    return 0;
}

void InputMessage(char *message)
{
    cin.getline(message, MAX_MESSAGE_LENGTH, '\n');
}

int PrepareClientSocket()
{
    int sock;
    struct sockaddr_in addr;
    printf("Creating socket...\n");
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0)
    {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(NETWORK_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    printf("Mounting connection...\n");
    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("connect");
        exit(2);
    }
    return sock;
}

void PrintMenu()
{
    printf("Choose type of Client:\n\t1. Sender\n\t2. Listener\n\tAny another key to exit\nYour answer: ");
}

void ExecuteClient()
{
    int sock = PrepareClientSocket();
    while (1)
    {
        PrintMenu();
        char input_key;
        input_key = getchar();
        switch (input_key)
        {
        case '1':
            ExecuteSender(sock);
            break;
        case '2':
            ExecuteListener(sock);
        default:
            break;
        }
        if ((input_key != '1') && (input_key != '2'))
        {
            printf("Bye!(%d)\n", input_key);
            break;
        }
    }
}

void ExecuteSender(int sock)
{
    char message[MAX_MESSAGE_LENGTH + 1];
    printf("Type something: ");
    InputMessage(message);//blank
    InputMessage(message);
    send(sock, message, sizeof(message), 0);
}

void ExecuteListener(int sock)
{
    printf("List of new messages: \n");
    char buf[MAX_MESSAGE_LENGTH + 1];
    int bytes_read = 1;
    while (bytes_read > 0)
    {
        bytes_read = recv(sock, buf, sizeof(buf), MSG_DONTWAIT);
        if (bytes_read > 0)
        {
            printf(buf);
        }
        printf("\n");
    }
    getchar();//blank
}