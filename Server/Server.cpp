 #include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include "string.h"
#include "sys/socket.h"
#include <netinet/in.h>
#include "unistd.h"
#include "fcntl.h"
#include <vector>
#include <set>
#include "errno.h"
#include <algorithm>

using namespace std;

#define MAX_MESSAGE_LENGTH 1024
#define MAX_CONNECTIONS_AMOUNT 5
#define NETWORK_PORT 3425

void ExecuteServer();
int PrepareServerListener();
void TransmitMessages(set<int> *clients, fd_set* readset);
void SendToOtherClients(int sender, set<int> clients, char message[MAX_MESSAGE_LENGTH + 1]);

int main()
{
    ExecuteServer();
    return 0;
}

void ExecuteServer()
{
    int listener = PrepareServerListener();
    set <int> clients;
    
    while(1)
    {
        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(listener, &readset);
        printf("Initialising size of clients set: %i\n", clients.size());
        for(set<int>::iterator it = clients.begin(); it != clients.end(); it++)
            FD_SET(*it, &readset);
        
        timeval timeout;
        timeout.tv_sec = 5;
        timeout.tv_usec = 0;

        int mx = max(listener, *max_element(clients.begin(), clients.end()));
        if (select(mx + 1, &readset, NULL, NULL, &timeout) <= 0)
        {
            if (errno != 0)
            {
                perror("select");
                exit(3);
            }
        }

        if(FD_ISSET(listener, &readset))
        {
            int sock = accept(listener, NULL, NULL);
            if(sock < 0)
            {
                perror("accept");
                exit(3);
            }

            fcntl(sock, F_SETFL, O_NONBLOCK);

            clients.insert(sock);
        }
        
        TransmitMessages(&clients, &readset);
    }
}

int PrepareServerListener()
{
    struct sockaddr_in addr;
    int listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        perror("socket");
        exit(1);
    }
    
    fcntl(listener, F_SETFL, O_NONBLOCK);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(NETWORK_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }

    listen(listener, MAX_CONNECTIONS_AMOUNT);
    return listener;
}

void TransmitMessages(set<int> *clients, fd_set* readset)
{
    char buf[MAX_MESSAGE_LENGTH + 1];
    int bytes_read;
    for (set<int>::iterator it = clients->begin(); it != clients->end(); it++)
        {
            if(FD_ISSET(*it, readset))
            {
                bytes_read = recv(*it, buf, MAX_MESSAGE_LENGTH + 1, 0);
                
                if(bytes_read <= 0)
                {
                    close(*it);
                    clients->erase(*it);
                    break;
                }
                SendToOtherClients(*(clients->find(*it)), *clients, buf);
            }
        }
}

void SendToOtherClients(int sender, set<int> clients, char message[MAX_MESSAGE_LENGTH + 1])
{
    for (set<int>::iterator it = clients.begin(); it != clients.end(); it++)
    {
        if (*clients.find(*it) != sender)
            send(*it, message, MAX_MESSAGE_LENGTH + 1, 0);
    }

}